#!/usr/bin/env python

from distutils.core import setup

with open('requirements.txt', 'rt') as f:
    install_requires = [l.strip() for l in f.readlines()]


setup(name='SpinalDiffusionTools',
      version='1.0.0',
      description='Spinal Diffusion Tools',
      author=['Saad Jbabdi'],
      author_email=['saad@fmrib.ox.ac.uk'],
      packages=['spinal_dmri_pipeline'],
      install_requires=install_requires,
      scripts=['spinal_dmri_pipeline/scripts/spinal_preproc.sh',
               'spinal_dmri_pipeline/scripts/spinal_models.sh',
               'spinal_dmri_pipeline/scripts/spinal_report.py',
               'spinal_dmri_pipeline/scripts/spinal_create_siemens_directions.py',
               'spinal_dmri_pipeline/scripts/fwdti.py']
     )
