# spinal_dmri_pipeline

Scripts for processing spinal cord dMRI data.

# Content:

- scripts
  - `spinal_preproc.sh`
  
    - Runs Topup/Eddy and gets the data ready for model fitting.
  
  - `spinal_models.sh`
 
    - Runs different flavours of DTI (standard, kurtosis, and free-water elimination). 
    - Runs bedpostX
    - Runs TDI

  - `spinal_create_siemens_file.py`

    - Creates a Siemens-style directions file

  - `make_report.py`
    - Creates HTML report 


# How to install

You need to make sure that you have [FSL](https://fsl.fmrib.ox.ac.uk/fsl/fslwiki) installed (version 6.0.5 or
higher).

You also need the [Spinal Cord Toolbox](https://spinalcordtoolbox.com) for one of the pre-processing
steps.

Next, you can install this pipeline using the below:

```bash
git clone https://git.fmrib.ox.ac.uk/saad/spinal_dmri_pipeline.git
cd spinal_dmri_pipeline
pip install .
```

# How to run

1. spinal_preproc.sh <dwi_file> <revPE_file> <T2file> <output_dir>

2. spinal_models.sh <output_dir>

3. make_report.py <output_dir>



# Dependencies
- FSL (6.0.5 or higher)
- Spinal Cord Toolbox
