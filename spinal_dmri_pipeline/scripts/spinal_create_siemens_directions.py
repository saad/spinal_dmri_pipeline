#!/usr/bin/env python
import numpy as np
import os
import argparse

# Helper functions                                                                                                                  


def generate_siemens(bvecs,bvals):
    #if bvecs.shape[0] != len(bvals):
    #    raise(Exception('Number of bvals and bvecs must match'))
    # Normalise bvecs                                                                                                               
    bvecs = bvecs / np.linalg.norm(bvecs,axis=1,keepdims=True)
    # Scale all bvecs such that the norm is 1 for the max bval                                                                      
    bvecs = bvecs * np.sqrt( bvals[:,None] / max(bvals) )
    # Produce the string                                                                                                            
    S  = f'[directions={len(bvals)}]\nCoordinateSystem = xyz\nNormalisation=none\n'
    for i,v in enumerate(bvecs):
        S += f'Vector[{i}] = ( {v[0]:1.5f}, {v[1]:1.5f}, {v[2]:1.5f}  )\n'
    return S

def savetxt(filename,textstring):
    with open(filename,'w') as f:
        f.write(textstring)

def run_gps(ndirs):
    tmp = os.popen('tmpnam').read().rstrip('\n') #'grot'
    cmd = f'/home/fs0/saad/bin/gps --ndir={",".join([str(d) for d in ndirs])} --out={tmp}'
    print('...Running GPS')
    print(cmd)
    os.system(cmd)    
    bvecs = [np.loadtxt(f'{tmp}_{i}_{j}.txt',ndmin=2) for i,j in zip(range(1,len(ndirs)+1),ndirs)]
    return bvecs



from itertools import chain, zip_longest
def ziplists(*args):
    return np.array([x for x in chain(*zip_longest(*args)) if x is not None])


def create_directions_file(bs,ndirs,filename,stagger=False,save_bfiles=False):
    # start by running gps                                                                                                          
    bvecs = run_gps(ndirs)    
    # create bvals                                                                                                                  
    bvals = [np.ones(i)*j for i,j in zip(ndirs,bs)]
    if stagger:
        bvals,bvecs = ziplists(*bvals),ziplists(*bvecs)
    else:
        bvals = np.concatenate(bvals)
        bvecs = np.concatenate(bvecs,axis=0)
    # save                                                                                                     
    #print(bvals)
    savetxt(filename, generate_siemens(bvecs,bvals))
    print(f'...Generated file {filename} with {bvecs.shape[0]} directions and {len(bs)} shells')

    if save_bfiles:
        np.savetxt(filename.rstrip('.txt')+'.bval',bvals)
        np.savetxt(filename.rstrip('.txt')+'.bvec',bvecs)
        


# MULTISHELL PROTOCOL FOR SPINAL CORD
# --> num dir not as important as num shells


#bs    = [5, 700, 1000, 1500, 2000]   # b-values
#ndirs = [4, 10,  20,   20,   20]     # directions per bvalue

#filename = '/home/fs0/saad/DirectionsSpinal_multishell.txt'


def main():
    p = argparse.ArgumentParser('Create Siemens Directions File')
    p.add_argument('-b','--bvals',  required=True,  type=float, help='b-shells',                   nargs='*')
    p.add_argument('-n','--ndirs',  required=True,  type=int,   help='directions per shell',       nargs='*')
    p.add_argument('-o','--out',    required=True,  type=str,   help='output file name')
    p.add_argument('--stagger',     required=False,             help='interleave the shells',      action='store_true')
    p.add_argument('--save_bfiles', required=False,             help='save FSL-style bvals/bvals', action='store_true')

    a = p.parse_args()
    if len(a.bvals) != len(a.ndirs) :
        raise(Exception('Numer of shells must match number of directions per shell'))

    create_directions_file(a.bvals,a.ndirs,a.out,a.stagger,a.save_bfiles)
    

if __name__ == '__main__':
    main()
    

