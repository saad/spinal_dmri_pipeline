#!/bin/bash

# Spinal Preproc Pipeline
# S Jbabdi 02/2022

if [ "$1" == "" ];then
    echo ""
    echo "Usage : spinal_pipeline.sh <dwidata> <revPE> <T2> <output_dir>"
    echo ""
    echo " dwidata    : 4D (main) dataset"
    echo " revPE      : either b0s with different PE or another 4D set with a different PE "
    echo " T2         : T2-weighted image used for generating a mask of the spinal cord"
    echo " output_dir : output directory"
    echo ""
    echo " IMPORTANT:  All the provided dwi NIFTIs must have a JSON side car AND a .bval side car"
    echo ""
    exit 1
fi

set -e

# Get file names but strip out the .nii.gz
datafile=`$FSLDIR/bin/imglob $1`
revPEfile=`$FSLDIR/bin/imglob $2`
T2file=`$FSLDIR/bin/imglob $3`
D=$4

diff_files="$datafile $revPEfile"

echo "Running on following input:"
echo "    data          : $datafile"
echo "    revPE         : $revPEfile"
echo "    T2            : $T2file"
echo "    output folder : $D"
echo ""

echo "Creating output folder structure"
mkdir -p $D/Topup
mkdir -p $D/Eddy
mkdir -p $D/Diffusion
mkdir -p $D/logs


# HELPER FUNCTIONS
# determine if input is AP or PA
get_PEdir(){
    PEdir=`cat ${1}.json | grep "PhaseEncodingDirection" | grep j | awk '{print $2}' | sed s@\"@@g | sed s@","@@g`    
    if [ "$PEdir" == "j" ];then
	echo AP 0 1 0
    elif [ "$PEdir" == "j-" ];then
	echo PA 0 -1 0	
    elif [ "$PEdir" == "i" ];then
	echo RL 1 0 0	
    elif [ "$PEdir" == "i-" ];then
	echo LR -1 0 0	
    elif [ "$PEdir" == "k-" ];then
	echo UD 0 0 1
    elif [ "$PEdir" == "k" ];then
	echo DU 0 0 -1
    else
	echo "Unkrecognised PE direction $PEdir"
	exit 1    
    fi
}

# find dwell time
dwell_time(){
    echo `cat ${1}.json | grep TotalReadoutTime | awk '{print $2}' | sed s@,@@g`
}
# find out whether to cut a slice or not
slices_even(){
    nslices=`fslval $1 dim3`
    if [ $((nslices%2)) -eq 0 ];then
	echo 1
    else
	echo 0
    fi
}
# get index of first b0 in a volume
idx_of_first_b0(){
    if [ ! -f ${1}.bval ];then
	echo 0
	return
    fi
    bvals=`cat ${1}.bval`
    idx=0
    for b in $bvals;do
	if [ "$b" -le "100" ];then
	    echo $idx
	    return
	fi
	let "idx+=1"
    done
}

# Extract and merge b0s

mergecmd="fslmerge -t $D/Topup/all_b0s_noclip"
echo "Extracting and merging b0s. Creating acqparam file"
echo ""
rm -rf $D/Topup/acqparams.txt
for f in $datafile $revPEfile;do    
    PEdir=`get_PEdir $f | awk '{print $1}'`
    PEline=`get_PEdir $f | awk '{print $2 " " $3 " " $4}'`
    idx=`idx_of_first_b0 $f`
    fslroi $f $D/Topup/b0s_$PEdir $idx 1
    dt=`dwell_time $f`
    nb0s=`fslnvols $D/Topup/b0s_$PEdir`
    for (( i = 1; i <= "$nb0s"; i++ ));do
	echo $PEline $dt >> $D/Topup/acqparams.txt
    done
    mergecmd="$mergecmd $D/Topup/b0s_$PEdir"
done
echo $mergecmd
`$mergecmd`


# Run Topup (need to remove one slice)
echo "Running Topup"
echo ""
if [ `slices_even $datafile` -eq 1 ];then
    immv $D/Topup/all_b0s_noclip $D/Topup/all_b0s
else
    roi="0 -1 0 -1 1 -1"
    fslroi $D/Topup/all_b0s_noclip $D/Topup/all_b0s $roi
fi

ID_topup=`fsl_sub -q veryshort.q -l $D/logs -N spinal_topup topup --imain=$D/Topup/all_b0s --datain=$D/Topup/acqparams.txt \
    --config=b02b0.cnf --out=$D/Topup/topup_results --verbose \
    --iout=$D/Topup/topup_results_hifi --fout=$D/Topup/topup_results_field`
echo " submitted. job:$ID_topup"

# Prepare for Eddy 
echo "Preparing for Eddy"
echo ""
# if revPE is just b0s    : don't concatenate
# if revPE is 4D with dwis: do concatenate
# --> just test if nvols is the same between dwidata and revPE
nvols=`fslnvols $datafile`
if [ "$nvols" == `fslnvols $revPEfile` ];then
    echo " Data is 2xPE --> Concatenate "
    fslmerge -t $D/Eddy/dwis $datafile $revPE
    paste ${datafile}.bval ${revPE}.bval > $D/Eddy/bvals
    paste ${datafile}.bvec ${revPE}.bvec > $D/Eddy/bvecs
    indx=""
    for ((i=1; i<=${nvols}; i+=1)); do indx="$indx 1"; done
    for ((i=1; i<=${nvols}; i+=1)); do indx="$indx 2"; done
    cp $D/Topup/acqparams.txt $D/Eddy/acqparams.txt
else
    echo " Data is 1xPE+revB0 --> Don't Concatenate "
    imcp $datafile $D/Eddy/dwis 
    cp ${datafile}.bval $D/Eddy/bvals
    cp ${datafile}.bvec $D/Eddy/bvecs
    indx=""
    for ((i=1; i<=${nvols}; i+=1)); do indx="$indx 1"; done
    head -n 1 $D/Topup/acqparams.txt > $D/Eddy/acqparams.txt
fi
echo $indx > $D/Eddy/index.txt

if [ `slices_even $datafile` -eq 0 ];then
    roi="0 -1 0 -1 1 -1"
    fslroi $D/Eddy/dwis $D/Eddy/dwis $roi
fi

# ---------- Get mask with SCT -------------------------------------------------------------------------------------
echo "Creating SC mask with SCT"
echo ""
mkdir -p $D/T2w
# copy input file
imcp ${T2file} $D/T2w/t2
sctbin=/vols/Scratch/saad/sct_5.4/bin
$sctbin/sct_deepseg_sc -i `$FSLDIR/bin/imglob -extension $D/T2w/t2.nii.gz` -c t2 -o $D/T2w/t2_seg_no_tumour.nii.gz

# Try different options
$sctbin/sct_deepseg_sc -i `$FSLDIR/bin/imglob -extension $D/T2w/t2.nii.gz` -c t2 -o $D/T2w/t2_seg_no_tumour.nii.gz -kernel 3d -qc $D/T2w -thr -1

# Label vertebrae
$sctbin/sct_label_vertebrae -i $D/T2w/t2.nii.gz -s $D/T2w/t2_seg_no_tumour.nii.gz -c t2 -ofolder $D/T2w


# When there is a tumour, try to detect it and add it to the mask
sct_deepseg -task seg_tumor_t2 -i `$FSLDIR/bin/imglob -extension $D/T2w/t2.nii.gz` -c t2 -o $D/T2w/t2_tumour_seg.nii.gz
fslmaths  $D/T2w/t2_seg_no_tumour.nii.gz -add $D/T2w/t2_tumour_seg.nii.gz -bin $D/T2w/t2_seg.nii.gz
dmri=$D/Eddy/dwis.nii.gz
$sctbin/sct_maths -i $dmri -mean t -o $D/Eddy/dmri_mean.nii.gz
$sctbin/sct_register_multimodal -i $D/T2w/t2_seg.nii.gz -d $D/Eddy/dmri_mean.nii.gz -identity 1 -x nn -ofolder $D/T2w
$sctbin/sct_create_mask -i $D/Eddy/dmri_mean.nii.gz -p centerline,$D/T2w/t2_seg_reg.nii.gz -size 35mm -o $D/Eddy/mask.nii.gz
# -------------------------------------------------------------------------------------------------------------------

echo "Running Eddy"
echo ""
ID_eddy=`fsl_sub -q cuda.q -l $D/logs -j $ID_topup -N spinal_eddy "eddy --imain=$D/Eddy/dwis --mask=$D/Eddy/mask \
    --acqp=$D/Eddy/acqparams.txt --index=$D/Eddy/index.txt \
    --bvecs=$D/Eddy/bvecs --bvals=$D/Eddy/bvals \
    --topup=$D/Topup/topup_results --out=$D/Eddy/eddy_corrected_data --verbose \
    --json=${datafile}.json --cnr_maps --residuals --estimate_move_by_susceptibility --repol;cp $D/Eddy/eddy_corrected_data.nii.gz $D/Diffusion/data.nii.gz;cp $D/Eddy/bvals $D/Diffusion/bvals;cp $D/Eddy/bvecs $D/Diffusion/bvecs;cp $D/Eddy/mask.nii.gz $D/Diffusion/nodif_brain_mask.nii.gz"`
echo " eddy submitted ID:$ID_eddy"

ID_quad=`fsl_sub -q veryshort.q -l $D/logs -j $ID_eddy -N spinal_quad eddy_quad $D/Eddy/eddy_corrected_data -idx $D/Eddy/index.txt -par $D/Eddy/acqparams.txt -m $D/Eddy/mask -b $D/Eddy/bvals -g $D/Eddy/bvecs -f $D/Topup/topup_results_field.nii.gz -v `
echo " quad submitted ID:$ID_quad"

# Generate QC figures here

# Generate report here

echo "Done."

