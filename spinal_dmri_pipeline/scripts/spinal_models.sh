#!/bin/bash

# Spinal Modelling Pipeline
# S Jbabdi 02/2022

if [ "$1" == "" ];then
    echo ""
    echo "Usage : spinal_models.sh <main_dir>"
    echo ""
    echo " main_dir : output folder of the preproc pipeline"
    echo ""
    exit 1
fi

set -e

D=$1

# Create out folders
echo "Create output folders"
mkdir -p $D/DTIFIT
mkdir -p $D/DKIFIT
mkdir -p $D/FW_DTIFIT
mkdir -p $D/TDI
mkdir -p $D/TDI_lr


# DTIFIT
fsl_sub -q veryshort.q -l $D/logs dtifit -k $D/Diffusion/data -m $D/Diffusion/nodif_brain_mask -r $D/Diffusion/bvecs -b $D/Diffusion/bvals -o $D/DTIFIT/dti --save_tensor
fsl_sub -q veryshort.q -l $D/logs -N dkifit "dtifit -k $D/Diffusion/data -m $D/Diffusion/nodif_brain_mask -r $D/Diffusion/bvecs -b $D/Diffusion/bvals -o $D/DKIFIT/kurt --save_tensor --kurt;fslmaths $D/DKIFIT/kurt_kurt -thr 0 -uthr 4 $D/DKIFIT/kurt_kurt"


# FW_DTIFIT
fsl_sub -q veryshort.q -l $D/logs fwdti.py --data $D/Diffusion/data \
    --mask $D/Diffusion/nodif_brain_mask \
    --bvecs $D/Diffusion/bvecs --bvals $D/Diffusion/bvals --out $D/FW_DTIFIT/fwdti

exit 1
# BedpostX
rm -rf $D/Diffusion.bedpostX
bedpostx_gpu $D/Diffusion -n 2 -b 5000 -model 1 
ID=`cat ${D}/Diffusion.bedpostX/logs/postproc_ID`

# TDI
tdi(){
    bpxdir=$1
    outres=$2
    outdir=$3
    frac=$4
    shift;shift;shift;shift
    opts=$@
    # FLIRT
    $FSLDIR/bin/flirt -in $bpxdir/nodif_brain_mask \
    -out $outdir/highresmask \
    -ref $bpxdir/nodif_brain_mask -applyisoxfm $outres -interp nearestneighbour
    # SUBSAMPLE
    if [ "$frac" != 1 ];then
	echo subsampling
	$FSLDIR/bin/fslmaths $outdir/highresmask -mul 0 -rand -mas $outdir/highresmask -uthr $frac -bin $outdir/highresmask
    fi
    # PTX
    o=""
    o="$o --opd --opathdir --forcedir -P 5 --sampvox=1 -S 20 --randfib=1 -m $bpxdir/nodif_brain_mask"
    o="$o --dir=${outdir} -s $bpxdir/merged -x $outdir/highresmask"    
    echo probtrackxing
    mkdir -p $outdir
    id=`fsl_sub -q cuda.q -l $D/logs -j $ID -N tdi probtrackx2_gpu $o $opts`
    echo $id
}
tdi $D/Diffusion.bedpostX .2 $D/TDI .25 -S 1000 --randfib=1 --sampvox=.3
# TDI lowres
ID_TDI_LR=`tdi $D/Diffusion.bedpostX .5 $D/TDI_lr .25 -S 1000 --randfib=1 --sampvox=.3`

# make into xtract-style output
# NOT WORKING YET 
# mkdir -p $D/tdi_lr/tracts/spinalcord
# cmd="fslmaths $D/tdi_lr/fdt_paths -div `cat $D/tdi_lr/waytotal` $D/tdi_lr/tracts/spinalcord/densityNorm"
# cmd="$cmd;imcp $D/tdi_lr/fdt_paths_localdir $D/tdi_lr/tracts/spinalcord/density_localdir"
# fsl_sub -q veryshort.q -l $D/logs -j $ID_TDI_LR -N post_tdi $cmd

echo "Done."
