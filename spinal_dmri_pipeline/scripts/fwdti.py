#!/usr/bin/env python

# Simple script for fitting Free-water eliminated DTI using DIPY

# Author: S. Jbabdi - 02/2022

import argparse

p = argparse.ArgumentParser("Fit FOD")
p.add_argument('--data',required=True,type=str,
                metavar='<IMAGE>',help='input DWI data')
p.add_argument('--bvecs',required=True,type=str,
                metavar='<FILE>',help='bvecs ASCII text file')
p.add_argument('--bvals',required=True,type=str,
                metavar='<FILE>',help='bvals ASCII text file')
p.add_argument('--out',required=True,type=str,
                metavar='<str>',help='output basename')
p.add_argument('--mask',required=True,type=str,
                metavar='<IMAGE>',help='brain mask')
p.add_argument('--verbose',action="store_true",
                help='print info')


# Parse Arguments here
args = p.parse_args()

# ------------ Other imports
import numpy as np
from fsl.data.image import Image

# ----- dipy imports ------ #
import dipy.reconst.fwdti as fwdti
from dipy.core.gradients import gradient_table


# --- Start here
# Get data
if args.verbose:
    print('Read data')
data   = Image(args.data)
header = data.header # store header info
data   = data.data   # get data array
# Brain mask
mask = Image(args.mask).data
# bvals, bvecs text to array
bvals = np.loadtxt(args.bvals)
bvecs = np.loadtxt(args.bvecs).T
bvecs[bvals<5] = [1,0,0] # fix in case bvecs=0,0,0
bvecs = bvecs / np.sqrt(np.sum(bvecs**2,axis=1,keepdims=True)) # normalise


# Set gradient table
gtab = gradient_table(bvals, bvecs)


# Fit ODF model
if args.verbose:
    print('Fit model')

fwdtimodel = fwdti.FreeWaterTensorModel(gtab)
fwdtifit   = fwdtimodel.fit(data, mask=mask)


if args.verbose:
    print('Save output')

outputlist = ['FA','AD','MD','MODE','RD','SPHERICITY','LINEARITY','PLANARITY','F']
for o in outputlist:
    img = Image(getattr(fwdtifit,o.lower()),header=header)
    img.save(f'{args.out}_{o}')


if args.verbose:
    print('Done')




