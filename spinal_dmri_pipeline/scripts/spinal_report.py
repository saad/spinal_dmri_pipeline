#!/usr/bin/env python


# Generate Figures for Report

# 1. Segmentation results (spinal + tumour if it is there)
# 2. Segmentation of vertebrae
# 3. Extract values from the diffusion / T2w data
# 4. Make the plots
# 5. Stick into HTML report or PDF (using plotly?)

import os
import numpy as np
import pandas as pd
from fsl.data.image import Image

from scipy.ndimage import center_of_mass

def get_cog(imagefile):
    img = Image(imagefile).data
    cog = [int(x) for x in center_of_mass(img)]
    cog_2d = [int(x) for x in center_of_mass(img[:,:,cog[-1]])]
    return f'{cog_2d[0]} {cog_2d[1]} {cog[-1]}'


def make_fsleyes_seg_cmd( background, segmentation, imgfile, label=False ):
    cmd = f'fsleyes render --outfile {imgfile} '
    cmd += f' --voxelLoc {get_cog(segmentation)} '    
    cmd += f'{background} -cm greyscale ' 
    if not label:
        cmd += f'{segmentation} -cm red -a 70.0'
    else:
        cmd += f'{segmentation} --overlayType label'
    return cmd

from fsl.transform.affine import transform, concat
from scipy.ndimage import map_coordinates
def get_values(datafile,spinefile,vertebraefile):
    # Slice-wise sampling of the data
    data    = Image(datafile)
    vert    = Image(vertebraefile)
    spine   = Image(spinefile)
    vox2fsl = vert.getAffine('voxel', 'world')
    fsl2vox = data.getAffine('world', 'voxel')
    vox2vox = concat(fsl2vox, vox2fsl)
        
    # spine voxels    
    values      = []
    indices     = []
    all_slices  = []
    for v in range(1,int(vert.data.max())):
        vox = np.asarray(np.nonzero(spine.data*(vert.data==v))).T
        slices = sorted(np.unique(vox[:,-1]),reverse=True)
        for sl in slices:
            voxels  = vox[vox[:,2]==sl, : ]
            datavox = np.asarray(transform(voxels,vox2vox))
            val     = map_coordinates(data.data,datavox.T,order=1).mean()
            values.append(val)    
            indices.append(v)
            all_slices.append(sl)
    return values, indices, all_slices
        
def get_vert_name(i):
    names = [f'C{i}' for i in range(1,8)]
    names.extend( [f'T{i}' for i in range(1,12)] )
    return names[i+1]

def create_df(subdir):
    datadict = {}
    spineseg     = os.path.join(subdir,'T2w','t2_seg')
    vertseg      = os.path.join(subdir,'T2w','t2_seg_labeled')
    datafile = os.path.join(subdir,'DTIFIT','dti_FA')
    _,vert,slices   = get_values(datafile,spineseg,vertseg)
    datadict['vertebrae'] = vert
    datadict['slice'] = slices

    datafile = os.path.join(subdir,'T2w','t2')
    t2,_,_  = get_values(datafile,spineseg,vertseg)
    datadict['T2w']       = t2

    datafile = os.path.join(subdir,'T2w','t2_tumour_seg')
    tum,_,_  = get_values(datafile,spineseg,vertseg)
    datadict['Tumour']       = tum

    measures = { 
        'dti' : ('DTIFIT',['MD','FA','S0']),
        'kurt' : ('DKIFIT',['kurt','FA','MD','S0']),
        'fwdti' : ('FW_DTIFIT',['AD','F','MD']),                   
    }

    for m in measures:
        for x in measures[m][1]:
            datafile = os.path.join(subdir,measures[m][0],f'{m}_{x}')
            vals,_,_   = get_values(datafile,spineseg,vertseg)
            datadict[f'{m}-{x}'] = vals

    return pd.DataFrame(datadict)

import plotly.express as px
def make_fig(df,meas):
    fig = px.scatter(df, x="slice", y=meas)

    for v in np.unique(df['vertebrae']):
        midsl = df['slice'][df['vertebrae']==v].mean()    
        minsl = df['slice'][df['vertebrae']==v].min()     
        maxsl = df['slice'][df['vertebrae']==v].max()    
        col   = ['white','black'][np.remainder(v,2)]
        tum = df['Tumour'][df['vertebrae']==v].mean()
        if tum>0.5 :
            col = 'red'
    
        fig.add_vline(x=minsl, line_width=1, line_dash="dot", line_color="black")
        fig.add_vrect(x0=minsl, x1=maxsl, line_width=0, fillcolor=col, 
                      opacity=0.1,annotation_text=get_vert_name(v), 
                      annotation_position="top left")
           
    fig.update_layout(template='ggplot2')
    # fig.show()
    return fig

import plotly


def to_div(fig):
    """
    Turns Plotly Figure into HTML
    """
    return plotly.offline.plot(fig,output_type='div',include_plotlyjs='cdn')


def generate_report(subdir):
    # Generate images
    # Report folder
    os.makedirs(os.path.join(subdir,'report'), exist_ok=True)

    # 1. Segmentation Results
    background   = os.path.join(subdir,'T2w','t2')
    spineseg     = os.path.join(subdir,'T2w','t2_seg')
    imagefile    = os.path.join(subdir,'report','spine_seg.png')
    cmd = make_fsleyes_seg_cmd( background, spineseg, imagefile )
    os.system(cmd)

    # 2. Certebrae Results
    background   = os.path.join(subdir,'T2w','t2')
    vertseg      = os.path.join(subdir,'T2w','t2_seg_labeled')
    imagefile    = os.path.join(subdir,'report','spine_vertebrae.png')
    cmd = make_fsleyes_seg_cmd( background, vertseg, imagefile, label=True )
    os.system(cmd)

    # 3. Values
    report_dir = os.path.join(subdir,'report')
    sections = []
    df = create_df(subdir)
    measures = { 
    'dti' : ('DTIFIT',['MD','FA','S0']),
    'kurt' : ('DKIFIT',['kurt','FA','MD','S0']),
    'fwdti' : ('FW_DTIFIT',['AD','F','MD']),                   
    }

    sections.append(
            f"""
            <h1><a name="model_vis">T2w</a></h1>
            <div id=fit>{to_div(make_fig(df,'T2w'))}</div>
            <hr>
            """)
    for m in measures:
        for x in measures[m][1]:
            meas=f'{m}-{x}'
            sections.append(
            f"""
            <h1><a name="model_vis">{meas}</a></h1>
            <div id=fit>{to_div(make_fig(df,meas))}</div>
            <hr>
            """)


    template = f"""<!DOCTYPE html>
        <html>
        <head>        
        <script src="https://cdn.plot.ly/plotly-latest.min.js"></script>
        </head>
        <body style="background-color:white">
        <div class="header">
        <center><h1>Spinal Report</h1></center>
        <hr>
        </div>
        <hr>
        <center>
        Spinal Cord Analysis Report
        </center>
        <hr>
        <body>
        <div>
        <h2>Spinal Cord Segmentation</h2>
        <img src={os.path.join(report_dir,'spine_seg.png')} alt="Italian Trulli">
        </div>
        <div>
        <h2>Vertebrae Segmentation</h2>
        <img src={os.path.join(report_dir,'spine_vertebrae.png')} alt="Italian Trulli">
        </div>
        """
        
    
        
    for section in sections:
        template += section

    # End of report
    template += """
    </body>
    </html>
    """

    # write
    with open(os.path.join(subdir,'report','report.html'), 'w') as f:
        f.write(template)


import argparse

def main():
    p = argparse.ArgumentParser('Create Spinal Report')
    p.add_argument('dir', type=str, help='analysis folder')
    
    a = p.parse_args()
    
    generate_report(a.dir)

if __name__ == '__main__':
    main()
    
